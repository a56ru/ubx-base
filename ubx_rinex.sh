#!/bin/bash

RNX_DATE=$(date -u -d "1 hour ago" +"%FT%T")
RNX_Y=$(date -d "$RNX_DATE" +%Y)
RNX_D=$(date -d "$RNX_DATE" +%j)
RNX_H=$(date -d "$RNX_DATE" +%H)

RECEIVER="$REC_NUM/$REC_TYPE/$REC_VERS"
ANTENNA="$ANT_NUM/$ANT_TYPE"
POSITION="$POS_X/$POS_Y/$POS_Z"
DELTA="$ANT_H/$ANT_E/$ANT_N"
KEEP_UBX=1

while getopts "y:d:h:a:o:m:n:t:c:r:e:v:p:d:k:" flag
do
	case "${flag}" in
		y) RNX_Y=${OPTARG};;
		d) RNX_D=${OPTARG};;
		h) RNX_H=${OPTARG};;
		a) AGENCY=${OPTARG};;
		o) OBSERVER=${OPTARG};;
		m) MARKER_NAME=${OPTARG};; 
		n) MARKER_NUMBER=${OPTARG};;
		t) MARKER_TYPE=${OPTARG};;
		c) MARKER_COUNTRY=${OPTARG};;
		r) RECEIVER=${OPTARG};;
		e) ANTENNA=${OPTARG};;
		v) RNX_VERSION=${OPTARG};;
		p) POSITION=${OPTARG};;
		d) DELTA=${OPTARG};;
		k) KEEP_UBX=1;;
		*) echo "invalid option or argument $OPTARG";;
	esac
done

for ubx in `find "${UBX_DIR}/${RNX_Y}/${RNX_D}" -iname "*${RNX_H}??.ubx" -type f`; do

convbin -r ubx -os -v $RNX_VERSION \
	-hm $MARKER_NAME \
	-hn $MARKER_NUMBER \
	-ht $MARKER_TYPE \
	-ho "$OBSERVER/$AGENCY" \
	-hr "$RECEIVER" \
	-ha "$ANTENNA" \
	-hp "$POSITION" \
	-hd "$DELTA" \
	-ti $RNX_INT \
	-d $RNX_DIR \
	$ubx 

if [ "x$KEEP_UBX" = "x0" ]; then
	rm -v $ubx
fi

done

#!/bin/bash

APP_HOME=/usr/local/share/asbru/ubx-base

# Set Date
UBX_DATE=$(date +"%Y-%m-%dT%H:%M:%S")
UBX_Y=$(date -d "$UBX_DATE" +%Y)
UBX_D=$(date -d "$UBX_DATE" +%j)
UBX_H=$(date -d "$UBX_DATE" +%H)
UBX_M=$(date -d "$UBX_DATE" +%M)
UBX_S=$(date -d "$UBX_DATE" +%S)

# Update ubx_date.env
cat <<EOF > $APP_HOME/ubx_date.env

# This is an automatically generated file
# Don't edit, your changes will be lost!

UBX_UPDATE=$UBX_DATE
UBX_Y=$UBX_Y
UBX_D=$UBX_D
UBX_H=$UBX_H
UBX_M=$UBX_M
UBX_S=$UBX_S

# --- End ---
EOF

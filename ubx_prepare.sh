#!/bin/bash

echo "UBX Receiver is going to be prepared"

# gpsctl -s 115200
ubxtool -d NMEA
ubxtool -d BEIDOU
ubxtool -d SBAS

ubxtool -e BINARY
ubxtool -e GPS
ubxtool -e GALILEO
ubxtool -e GLONASS
ubxtool -e RAWX

echo "UBX Receiver is now prepared"

# ubxtool -p CFG-GNSS
